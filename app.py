import os
from time import sleep
import re
import json
from flask import Flask, jsonify, request
import boto3

from openapi_core import create_spec
from openapi_core.shortcuts import RequestValidator
from openapi_core.contrib.flask import FlaskOpenAPIRequest
from yaml import load, Loader, dump, Dumper

ACCESS_ID = 'AKIA4KYMXBF6PM2QMAHX'
ACCESS_KEY = 'Eh7PidbgaBiMdoRlRNbNQ7djlTz0GITDJ4g3Mru1'
DeliveryStreamName="stream-1"
region_name='eu-west-2'

########################################
# open the specification and initialize the request data validator
########################################
with open('swagger.yml','r') as spec:
    spec_dict = load(spec.read(), Loader)

spec = create_spec(spec_dict)
validator = RequestValidator(spec)

########################################
# Begin webserver 

app = Flask(__name__)

@app.route('/track', methods=['POST'])
@app.route('/alias', methods=['POST'])
@app.route('/profile', methods=['POST'])
def validate_store():
    #validate request against the specification
    openapi_request = FlaskOpenAPIRequest(request)
    result = validator.validate(openapi_request)
    
    # get list of errors
    errors = [str(error) for error in result.errors]

    # return now if request is invalid
    if len(errors) != 0:
        return json.dumps(errors), 400, {'ContentType':'application/json'}

    # get dict from request
    request_dict = request.get_json()
    
    # connect to firehose
    firehose = boto3.client('firehose', region_name=region_name, \
        aws_access_key_id=ACCESS_ID, \
        aws_secret_access_key= ACCESS_KEY)
    request_data = json.dumps(request_dict).encode('utf-8')
    
    # put the data on firehose
    firehose.put_record(\
        DeliveryStreamName=DeliveryStreamName, Record={'Data': request_data})
    
    return json.dumps('OK'), 200, {'ContentType':'application/json'}

# End webserver 
########################################

if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=80)
