import boto3
import os
from time import sleep
import re
import json
from subprocess import PIPE, Popen

ACCESS_ID = 'AKIA4KYMXBF6PM2QMAHX'
ACCESS_KEY = 'Eh7PidbgaBiMdoRlRNbNQ7djlTz0GITDJ4g3Mru1'
DeliveryStreamName="stream-1"
region_name='eu-west-2'
host='3.9.172.0'
port=80

def build_test(host, port, endpoint, dictionary):
    '''
    builds a request to the server in the form of a curl
    '''
    load = (host, str(port), endpoint, dictionary)
    return '''curl -X POST http://%s:%s/%s -d \'%s\' -H "Content-Type:application/json"'''%load

########################################
# declare test cases
########################################
tests = {
    "track_pass_dict": ('track', r'''{"userId": "track", "events": [{"eventName": "string", "metadata": {}, "timestampUTC": 0}]}'''),
    "track_fail_dict": ('track', r'''{"userId": "track", "events": [{"eventName": 5,        "metadata": {}, "timestampUTC": 0}]}'''),
    "alias_pass_dict": ('alias', r'''{ "newUserId": "alias", "originalUserId": "string", "timestampUTC": 0}'''),
    "alias_fail_dict": ('alias', r'''{ "newUserId": "alias", "originalUserId": "string"                   }'''),
    "profile_pass_dict": ('profile', r'''{"userId": "profile", "attributes": {}, "timestampUTC": 0        }'''),
    "profile_fail_dict": ('profile', r'''{"userId": "profile", "attributes": {}, "timestampUTC": "profile"}'''),
}

########################################
# execute tests
########################################
for test in tests:
    print('''
    ###################
    STARTING TEST %s
    ###################
    '''%test)

    # print original dict
    print('original dict ', tests[test][1])

    ########################################
    # test inserting data in server
    ########################################
    endpoint = tests[test][0]
    test_dict = tests[test][1]
    test_str = build_test(host, port, endpoint, test_dict)
    result, _ = Popen(test_str, shell=True, stdout=PIPE, stderr=PIPE).communicate()
    result = result.decode('utf-8')
    print('test app result ', result)
    
    # if result is not OK, input data was wrong, continue to next test case
    if 'OK' not in result:
        continue

    ########################################
    # check if data inserted in server was also inserted in bucket
    ########################################
    
    # sleep 65 seconds, connect to the bucket, and check that the data is there.
    # firebase only inserts data in the bucket after a set amount of time, 
    # in this case, it's 60 seconds
    print('sleeping for 65 seconds before checking bucket')
    sleep(65)
    s3 = boto3.resource('s3', region_name=region_name, \
        aws_access_key_id=ACCESS_ID, aws_secret_access_key= ACCESS_KEY)
    bucket = s3.Bucket('bucket-buedaswag')

    # Amazon S3 lists objects in alphabetical order     
    bucket_dicts = [obj for obj in bucket.objects.all()][-1].\
        get()['Body'].read().decode('utf-8')
    bucket_dicts = '[' + re.sub('}{', '}, {', bucket_dicts) + ']'
    bucket_dicts = json.loads(bucket_dicts)

    # check if sent record is stored in the bucket
    test_json = json.loads(test_dict)
    print('test_json', test_json)
    print('bucket_dicts', bucket_dicts)
    print('DATA STORED IN S3 BUCKET: ', test_json in bucket_dicts)