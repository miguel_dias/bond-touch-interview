# Challenge Report

## What was done

* Deployed a flask server (app.py) in ec2 that receives post requests, validates them against the swagger specification, then sends the data to the firehose, which in turn stores it in s3
* Made a python script (test.py) that implements end-to-end tests on the server:
    * sends valid and invalid requests to the server
    * for the valid requests, checks if they got inserted in the s3 bucket 

## Timeline

### DAY 1
9:30
<br />Read the challenge
<br />10:00
<br />started setting up:
* Ec2
* Aws cli
* Aws IAM
* Firehose

13:00 until 14:20 
<br />Lunch
<br />14:46 
<br />Started using aws python api (boto3) to insert record in firehose.
Checked that the records were being inserted in s3 through s3 GUI.
<br />18:14
<br />Used openapi-core python library to validate http requests against the swagger specification.
<br />19:57
<br />Deployed the application to the cloud. At this stage it does the following:
* Receives http post requests
* validates them against the swagger specification
* sends the data to the firehose, which in turn stores it in s3
* Tested the application making curl requests in the command line.

20:00
<br />Wrote a todo list for the next day and talked with Sam about what was done so far.
<br />Todo for tomorrow:
* Make tests that check if data was inserted in the bucket
* Make tests with more cases
* Does this sound appropriate? Any suggestions?
* Clean code and code documentation
* Make challenge report
* Start on Bonus tasks

### DAY 2 
10:30
<br />Start writing end to end tests.
<br />13:00
<br />Local end to end test done, will generate test cases for invalid and run them in the cloud.
<br />14:00 -15:20
<br />Lunch
<br />16:47
<br />Cleaned up the server code, completed test cases.
<br />17:11
<br />Finished deploying server and tests, tested server, getting started on the challenge report.
<br />19:00
<br />Final tests and finish report.

## Testing the server

### With test.py

This is a complete end-to-end test of the server, making post requests and checking if the data was inserted to s3.<br />
To run in, install the conda environment in environment.yml<br />
The test is configured to run make requests to the ec2 server.

### With curl

Theese tests will validate that the server will tell if the requests are according to the specification

curl -X POST http://localhost:80/track -d '{"userId": "track", "events": [{"eventName": "string", "metadata": {}, "timestampUTC": 0}]}' -H "Content-Type:application/json"
<br />curl -X POST http://localhost:80/track -d '{"userId": "track", "events": [{"eventName": 5       , "metadata": {}, "timestampUTC": 0}]}' -H "Content-Type:application/json"
<br />curl -X POST http://localhost:80/alias -d '{ "newUserId": "alias", "originalUserId": "string", "timestampUTC": 0}' -H "Content-Type:application/json"
<br />curl -X POST http://localhost:80/alias -d '{ "newUserId": "alias", "originalUserId": "string"                   }' -H "Content-Type:application/json"
<br />curl -X POST http://localhost:80/profile -d '{"userId": "profile", "attributes": {}, "timestampUTC": 0        }' -H "Content-Type:application/json"
<br />curl -X POST http://localhost:80/profile -d '{"userId": "profile", "attributes": {}, "timestampUTC": "profile"}' -H "Content-Type:application/json"
